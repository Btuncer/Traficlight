
struct{
unsigned long premillis;
unsigned long cnt;
bool state;
bool prestate;
bool longpress;
bool shortpress;
int interval;
int pin;
}input;

int mode = 0;
int redpin = 2;
int yellowpin = 3;
int greenpin = 4;

void setup() {
  // put your setup code here, to run once:
  input.interval = 10;
  input.pin = 5;
  Serial.begin(9600);
  pinMode(input.pin, INPUT);
  pinMode(redpin,OUTPUT);
  pinMode(yellowpin,OUTPUT);
  pinMode(greenpin,OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:

   if(millis() - input.premillis > input.interval)
  {
    //input.state =~ digitalRead(input.pin);
    bool inputst;
    
    inputst = digitalRead(input.pin)== 0 ? 1 : 0;
    input.state = inputdebounce(inputst);

   /* Serial.print("in= ");
    Serial.print(inputst);
    Serial.print("debounce= ");
    Serial.println(input.state);*/

    manualmode();


    input.prestate = input.state;
    input.premillis = millis();
  }

}

/*
*
*End of loop
*
*/


int cntlt = 0;
void manualmode(void)
{
  if(risingedgedetect(input.state))
  {
    cntlt++;
  }
  switch (cntlt)
  {
    case 0:
    {
      redlight();
      break;
    }
    case 1:
    {
      yellowlight();
      break;
    }
    case 2:
    {
      greenlight();
      break;
    }
    case 3:
    {
      yellowlight();
      break;
    }
    default:
    {
      cntlt = 0;
      break;
    }

  }
  

}

struct {
  bool pre_state;
  bool output;
}rising;

bool risingedgedetect(bool in)
{

  if(in == 1 && rising.pre_state == 0)
  {
    rising.output = 1 ;
  }
  else{
    rising.output = 0;
  }  
rising.pre_state = in;
return rising.output;
}



struct{
  unsigned long cnt1;
  unsigned long cnt2;
  bool state;
  bool pre_state;

}debounce;
bool inputdebounce(bool button)
{
  
  
  if(button == 1)
  {
    
    if(debounce.cnt1++ > 10)
    {
      debounce.cnt2 = 0;
      debounce.state = 1;   
    }
  }
  else if(button == 0)
  {
    if(debounce.cnt2++ > 10)
    {
      debounce.cnt1 = 0;
      debounce.state = 0;
    }
  }

  return debounce.state;
}

void redlight(void)
{
  digitalWrite(redpin,HIGH);
  digitalWrite(yellowpin,LOW);
  digitalWrite(greenpin,LOW);
}

void yellowlight(void)
{
  digitalWrite(redpin,LOW);
  digitalWrite(yellowpin,HIGH);
  digitalWrite(greenpin,LOW);
}

void greenlight(void)
{
  digitalWrite(redpin,LOW);
  digitalWrite(yellowpin,LOW);
  digitalWrite(greenpin,HIGH);
}

void nolight(void)
{
  digitalWrite(redpin,LOW);
  digitalWrite(yellowpin,LOW);
  digitalWrite(greenpin,LOW);
}